<?php

namespace App\Presenters;

use Nette,
    \App\Model\Candidates as Candidates,
    \Nette\Application\UI\Form as Form;

/**
 * Description of CandidatesPresenter
 *
 * @author Daniel
 */
class CandidatesPresenter extends BasePresenter {

    /**
     * @var \App\Model\Candidates
     * @inject
     */
    public $candidates;

    /** @persistent */
    public $id_category;

    const SENDER = '1';

    public function renderDefault($id_category = NULL) {
        $this->template->categories = $this->candidates->getCategories();
        $interests = $this->candidates->getInterests($id_category);
        $this->template->interestsAll = $this->candidates->getInterests();
        if (!$interests) {
            throw new BadRequestException('Page not found');
        }
        $this->template->interests = $interests;
        $this->template->id_category = $id_category;
        $this->id_category = $id_category;
        $this->template->candidates = $this->candidates;
        $this->template->sender = self::SENDER;
        $paginator = new Nette\Utils\Paginator;
    }

    public function handleRate($id_candidate, $rating) {
        //\Tracy\Dumper::dump($rating);
        $this->candidates->rate($id_candidate, $rating, self::SENDER);
        if ($this->isAjax) {
            $this->redrawControl('interests');
        }
    }

    public function handleMove($id_interest, $id_new_category) {
        $interest = $this->candidates->getInterest($id_interest);
        if (!$interest) {
            throw new BadRequestException('Candidate not found');
        }
        $category = $this->candidates->getCategory($id_new_category);
        if (!$category) {
            throw new BadRequestException('Category not found');
        }
        $interest->update(
                array(
                    "id_category" => $id_new_category
        ));
        //$this->template->interests = $this->isAjax() ? array() : $this->candidates->getInterests($this->id_category);
        //$this->template->interests[$id_interest] = $this->candidates->getInterest($id_interest);
        $this->template->interests = $this->candidates->getInterests($this->id_category);
        //$this->template->id_category = $this->id_category;
        if ($this->isAjax) {
            $this->redrawControl('interests');
            $this->redrawControl('categories');
        }
    }

    protected function createComponentContactForm() {
        $form = new Form;
        $form->addTextArea('mail', 'Mail:')
                ->addRule(Form::MAX_LENGTH, 'Mail má maximálně %d znaků', 2000)
                ->setAttribute('cols', 60)
                ->setAttribute('maxlength', 2000)
                ->setAttribute('placeholder', 'max. 2000 znaků')
                ->setAttribute('rows', 10)
                ->setAttribute('wrap', 'soft')
                ->setAttribute('spellcheck', 'true')
                ->setRequired("Please fill-in e-mail text");
        $form->addHidden('mails');
        $form->addSubmit('send', 'Odeslat');
        $form->getElementPrototype()->class('form-horizontal'); // ajax
        $form = $this->bootstrapForm($form);
        $form->onSuccess[] = callback($this, 'contactFormSubmitted');

        return $form;
    }

    public function contactFormSubmitted(Form $form) {
        \Tracy\Dumper::dump($form->getValues());
    }

    /**
     * sice sem nepatri, ale at kvuli ni nemusim vytvaret tridu
     * @param \App\Presenters\UI\Form $form
     * @return \App\Presenters\UI\Form
     */
    public function bootstrapForm(Form $form) {
        // setup form rendering
        //$form->addProtection();
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class=col-sm-9';
        $renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';

        // make form and controls compatible with Twitter Bootstrap
        $form->getElementPrototype()->class('form-horizontal');

        foreach ($form->getControls() as $control) {
            if ($control instanceof Controls\Button) {
                $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
                $usedPrimary = TRUE;
            } elseif ($control instanceof Controls\TextBase || $control instanceof Controls\SelectBox || $control instanceof Controls\MultiSelectBox) {
                $control->getControlPrototype()->addClass('form-control');
            } elseif ($control instanceof Controls\Checkbox || $control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
            }
        }

        return $form;
    }

}
