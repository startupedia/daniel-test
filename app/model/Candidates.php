<?php

namespace App\Model;

use Nette;

/**
 * Description of Candidates
 *
 * @author Daniel
 */
class Candidates extends Nette\Object {

    /** @var Nette\Database\Context */
    private $database;

    /**
     * @inject @var \Nette\Caching\IStorage
     * 
     */
    private $storage;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    /**
     * 
     * @return type all candidate categories
     */
    public function getCategories() {
        $categories = $this->database->table("category");
        //\Tracy\Dumper::dump($categories);
        return $categories;
    }

    /**
     * 
     * @return type all interests for the position
     */
    public function getInterests($id_category = NULL) {
        if ($id_category) {
            $category = $this->database->table("category")->get($id_category);
            if (!$category) {
                return null;
            }
            $interests = $category->related("interest.id_category");
        } else {
            $interests = $this->database->table("interest");
        }
        //\Tracy\Dumper::dump($interests);
        return $interests;
    }

    public function getCandidate($id_candidate) {
        $candidate = $this->database->table("candidate")->get($id_candidate);
        return $candidate;
    }

    public function getCategory($id_category) {
        $category = $this->database->table("category")->get($id_category);
        return $category;
    }

    public function getInterest($id_interest) {
        $interest = $this->database->table("interest")->get($id_interest);
        return $interest;
    }

    public function rate($id_candidate, $rating, $sender) {
        if (!$this->getCandidate($id_candidate)) {
            throw new \Nette\Application\BadRequestException('Candidate not found.');
        }
        if ($this->alreadyRated($id_candidate, $sender)) {
            throw new \Nette\Application\BadRequestException('You can only rate one candidate one time.');
        }
        $table = $this->database->table("rating");
        //$datetime=  date($format);
        $table->insert(array(
            'datetime' => 'NOW()',
            'id_candidate' => $id_candidate,
            'stars' => $rating,
            'id_sender' => $sender
        ));
    }

    /**
     * 
     * @param type $id_candidate
     * @param type $sender
     * @return type true, pokud uz hodnotil
     */
    public function alreadyRated($id_candidate, $sender) {
        return count($this->database->table("rating")->where('id_candidate = ? AND id_sender = ?', $id_candidate, $sender)) > 0;
    }

}
