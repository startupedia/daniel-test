
$(function() {
    $.nette.init();
    $.nette.ext({
        before: function() {

        },
        start: function() {

        },
        success: function() {

        },
        error: function() {

        },
        complete: function() {
            countSelected();
        }
    });
    countSelected();
});
$(document).on("change", '#check-all', function(e) {
    var p = jQuery.noConflict();
    var inputs = p(".table.candidates input.interest-check");
    var links = p('.move-interest a');
    var selectorRows = '.table.candidates tr';
    var rows = p(selectorRows);
    //inputs.toggle(this.prop('checked'));
    if (this.checked) {
        inputs.attr('checked', true);
        links.addClass('selected');
        rows.addClass('selected');
    } else {
        inputs.attr('checked', false);
        links.removeClass('selected');
        rows.removeClass('selected');
    }
    countSelected();
});

$(document).on("change", '.interest-check', function(e) {
    var p = jQuery.noConflict();
    var i = p(this).attr('id');
    var id = i.substring(6);
    var selectorLinks = '#interest-' + id + ' .move-interest a';
    var links = p(selectorLinks);
    var selectorRows = '#interest-' + id;
    var rows = p(selectorRows);
    if (this.checked) {
        links.addClass('selected');
        rows.addClass('selected');
    } else {
        links.removeClass('selected');
        rows.removeClass('selected');
    }
    countSelected();
});
$(document).on("click", '.contact-interest', function(e) {
    var p = jQuery.noConflict();
    var i = p(this).attr('id');
    var id = i.substring(17);
    var selector = '#dialog-' + id;
    p(selector + ' #frm-contactForm-mails').attr('value', id);
    dialog(selector);
});
$(document).on("click", '#collective-response', function(e) {
    var p = jQuery.noConflict();
    var selector = '#dialog-col';
    var mails = '';
    var selectorRows = '.table.candidates tr.selected';
    var rows = p(selectorRows);
    if (!rows.length) {
        alert('You must choose at least one candidate');
    } else {
        rows.each(function() {
            var i = p(this).attr('id');
            var id = i.substring(9);
            mails = mails + id + ',';
        });
        //p('<input type="hidden" value = "' + mails + '" name="mails" id="frm-contactForm-mails"/>').appendTo('#frm-contactForm');
        p('#frm-contactForm-mails').attr('value', mails);
        //p('#frm-contactForm').
        dialog(selector);
    }
});
function dialog(selector) {
    var p = jQuery.noConflict();
    p(selector).dialog({
        modal: true,
        //autoOpen: false,
        //maxWidth: 740,
        //maxHeight: 440,
        width: 670,
        height: 340,
        open: function() {
            p('.ui-widget-overlay').bind('click', function() {
                p(selector).dialog('close');
            })
        },
    });
}
function countSelected() {
    var p = jQuery.noConflict();
    var inputsCount = p(".table.candidates input[type='checkbox']:checked").length;
    var badges = p('.selected-count');
    badges.text(inputsCount);
}

$(document).on("click", '.colMove', function(e) {
    var p = jQuery.noConflict();
    var i = p(this).attr('id');
    var id = i.substring(8);
    var selector = '.selected.category-' + id;
    var buttons = p(selector);
    buttons.appendTo('#buttonContainer');
    buttons.each(function(index) {
        p.get(p(this).attr('href'));
    });
    buttons.last().trigger('click');
    p('#buttonContainer').empty();
});

/*$(document).on("mouseover", '.rate', function(e) {
 var p = jQuery.noConflict();
 var id = p(this).attr('id');
 var rating = id.substring(id.lastIndexOf("-") + 1);
 var starspan = p(this).parent();
 var stars = starspan.find('a').slice(0, rating).find('span');
 stars.removeClass('glyphicon-star-empty');
 stars.addClass('glyphicon-star');
 var emptyStars = starspan.find('a').slice(rating + 1).find('span');
 emptyStars.removeClass('glyphicon-star-empty');
 emptyStars.addClass('glyphicon-star');
 });
 
 $(document).on("mouseover", '.rate', function(e) {
 var p = jQuery.noConflict();
 var starspan = p(this).parent();
 starspan.find('a').addClass('glyphicon-star-empty');
 starspan.find('a').removeClass('glyphicon-star');
 starspan.find('a.not-empty-before').removeClass('glyphicon-star-empty');
 starspan.find('a.not-empty-before').addClass('glyphicon-star');
 });*/
$(document).on("mouseover", 'span.star', function(e) {
    var p = jQuery.noConflict();
    p(this).addClass('hovered');
});

$(document).on("mouseover", 'span.star', function(e) {
    var p = jQuery.noConflict();
    p(this).removeClass('hovered');
});