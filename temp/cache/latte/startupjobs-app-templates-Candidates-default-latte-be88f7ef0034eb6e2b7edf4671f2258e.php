<?php
// source: C:\wamp\www\startupjobs\app/templates/Candidates/default.latte

// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('8111311087', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb82c6b05377_content')) { function _lb82c6b05377_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?><div class="container">
    <div class="row">
        <div class="col-md-5 hidden-xs">
        </div>
        <div class="col-md-6 col-xs-12">
            <nav class="nav nav-pills aligned">
                <p class="navbar-text navbar-left">Interested in the position</p>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle border pos-select" data-toggle="dropdown" aria-expanded="false">
                        HTML Kodér se znalostí PHP (<?php echo Latte\Runtime\Filters::escapeHtml(count($interestsAll), ENT_NOQUOTES) ?>) <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">1</a></li>
                    </ul>
                    <button type="button" class="btn btn-default">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default">
                        <span class="glyphicon glyphicon-compressed" aria-hidden="true"></span>
                    </button>
                </div>
            </nav>
        </div>
        <div class="col-md-1 hidden-xs">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-3 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Progress<a><span class="glyphicon glyphicon-cog right-align" aria-hidden="true"></a></div>
                <ul class="list-group bg-blue"<?php echo ' id="' . $_control->getSnippetId('categories') . '"' ?>>
<?php call_user_func(reset($_b->blocks['_categories']), $_b, $template->getParameters()) ?>
                </ul>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <nav class="navbar navbar-default">
                        <ul class="nav navbar-nav navbar-left padding">
                            <li>
                                                                <input type="checkbox" id="check-all">
                            </li>
                            <li>
                                <button class="btn btn-success" id="collective-response" type="button">
                                    Collective response <span class="badge selected-count">2</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Move <span class="badge selected-count"></span><span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
<?php $iterations = 0; foreach ($categories as $category) { ?>                                    <li>
                                        <a id="colMove-<?php echo Latte\Runtime\Filters::escapeHtml($category->id_category, ENT_COMPAT) ?>" class="colMove">
                                            <?php echo Latte\Runtime\Filters::escapeHtml($category->name, ENT_NOQUOTES) ?>

                                        </a>
                                    </li>
<?php $iterations++; } ?>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right padding">
                            <li>
                                <span>
                                    1 - 10 / 28
                                </span>
                            </li>
                            <li>
                                <button type="button" class="btn btn-link bg-blue">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-link bg-blue">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
                                </button>
                            </li>
                        </ul>
                    </nav>
                </div>
                                <div id="buttonContainer" class="invisible"></div>
                <div id="dialog-col" class="dialog" title="Contact candidate">
<?php $_l->tmp = $_control->getComponent("contactForm"); if ($_l->tmp instanceof Nette\Application\UI\IRenderable) $_l->tmp->redrawControl(NULL, FALSE); $_l->tmp->render() ?>
                </div>
                <table class="table candidates">
                    <tbody<?php echo ' id="' . $_control->getSnippetId('interests') . '"' ?>>
<?php call_user_func(reset($_b->blocks['_interests']), $_b, $template->getParameters()) ?>
                    </tbody>
                </table>
                <nav>
                    <ul class="pagination">
                        <li class="disabled">
                            <span>
                                <span aria-hidden="true">&laquo;</span>
                            </span>
                        </li>
                        <li class="active">
                            <span>1 <span class="sr-only">(current)</span></span>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div><?php
}}

//
// block _categories
//
if (!function_exists($_b->blocks['_categories'][] = '_lbb0c08ac791__categories')) { function _lbb0c08ac791__categories($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v; $_control->redrawControl('categories', FALSE)
?>                    <li class="list-group-item">
                        <a class="<?php if (!$id_category) { ?>active<?php } ?>" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Candidates:", array(null)), ENT_COMPAT) ?>
">
                            All candidates
                        </a> <span class="badge"><?php echo Latte\Runtime\Filters::escapeHtml(count($interestsAll), ENT_NOQUOTES) ?></span>
                    </li>
<?php $iterations = 0; foreach ($categories as $category) { ?>                    <li class="list-group-item">
<?php if ($id_category) { $cat=$categories->get($id_category) ;$ident_category=$cat->ident ;} else { $ident_category="" ;} ?>
                        <a class="<?php if ($ident_category==$category->ident) { ?>
active<?php } ?> <?php echo Latte\Runtime\Filters::escapeHtml($category->ident, ENT_COMPAT) ?>
" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Candidates:default", array($category->id_category)), ENT_COMPAT) ?>
">
                            <?php echo Latte\Runtime\Filters::escapeHtml($category->name, ENT_NOQUOTES) ?>

                        </a> <span class="badge"><?php echo Latte\Runtime\Filters::escapeHtml(count($category->related("interest.id_category")), ENT_NOQUOTES) ?></span>
                    </li>
<?php $iterations++; } 
}}

//
// block _interests
//
if (!function_exists($_b->blocks['_interests'][] = '_lbbcd479886b__interests')) { function _lbbcd479886b__interests($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v; $_control->redrawControl('interests', FALSE)
;$iterations = 0; foreach ($interests as $interest) { ?>                        <tr  id="interest-<?php echo Latte\Runtime\Filters::escapeHtml($interest->id_interest, ENT_COMPAT) ?>">
<?php $candidate=$interest->ref("candidate","id_candidate") ;$person=$candidate->ref("person","id_person") ;$category=$interest->ref('category','id_category') ?>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="check-<?php echo Latte\Runtime\Filters::escapeHtml($interest->id_interest, ENT_COMPAT) ?>" class="interest-check">
                                    </label>
                                </div>
                            </td>
                            <td>
                                <img src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($person->img_url), ENT_COMPAT) ?>" width="36px" height="36px" class="img-circle profile-pic">
                            </td>
                            <td>
                                <p>
<?php $rated = $candidates->alreadyRated($candidate->id_candidate, $sender) ?>
                                    <span class="candidate_name"><?php echo Latte\Runtime\Filters::escapeHtml($person->f_name, ENT_NOQUOTES) ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($person->l_name, ENT_NOQUOTES) ?></span>
<?php $rating=round(($candidate->related("rating.id_candidate")->aggregation("AVG(stars)"))) ?>
                                    <span class="rate <?php if ($rated) { ?>rated<?php } ?>
" id="rate-<?php echo Latte\Runtime\Filters::escapeHtml($candidate->id_candidate, ENT_COMPAT) ?>">
<?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <a <?php if (!$rated) { ?>href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("rate!", array($candidate->id_candidate, $i)), ENT_COMPAT) ?>
"<?php } ?>

                                                           class="ajax rate rate-<?php echo Latte\Runtime\Filters::escapeHtml($i, ENT_COMPAT) ?>" 
                                                           id="rate-<?php echo Latte\Runtime\Filters::escapeHtml($candidate->id_candidate, ENT_COMPAT) ?>
-<?php echo Latte\Runtime\Filters::escapeHtml($i, ENT_COMPAT) ?>">
<?php if ($rating>=$i) { ?>
                                                    <span class="glyphicon glyphicon-star star not-empty-before" aria-hidden="true" 
                                                          id="star-<?php echo Latte\Runtime\Filters::escapeHtml($candidate->id_candidate, ENT_COMPAT) ?>
-<?php echo Latte\Runtime\Filters::escapeHtml($i, ENT_COMPAT) ?>"></span>
<?php } else { ?>
                                                    <span class="glyphicon glyphicon-star-empty star" aria-hidden="true" 
                                                          id="star-<?php echo Latte\Runtime\Filters::escapeHtml($candidate->id_candidate, ENT_COMPAT) ?>
-<?php echo Latte\Runtime\Filters::escapeHtml($i, ENT_COMPAT) ?>"></span>
<?php } ?>
                                            </a>
<?php } ?>
                                    </span>
                                </p>
                                <p class="small">
                                    <span class="submit-date">Submitted <?php echo Latte\Runtime\Filters::escapeHtml($template->date($interest->submit_date, 'j. n. Y'), ENT_NOQUOTES) ?></span> | 
                                    <span class="status <?php echo Latte\Runtime\Filters::escapeHtml($category->ident, ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($category->name, ENT_NOQUOTES) ?></span>
                                </p>
                            </td>
                            <td class="right-align">
                                <div class="btn-group padding" role="group">
                                    <button type="button" class="btn btn-link bg-blue hover-only">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    </button>
                                    <button type="button" class="btn btn-default dropdown-toggle bg-blue hover-only" data-toggle="dropdown" aria-expanded="false">
                                        <?php echo Latte\Runtime\Filters::escapeHtml($category->name, ENT_NOQUOTES) ?> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu move-interest" role="menu">
<?php $iterations = 0; foreach ($categories as $category) { ?>                                        <li>
                                            <a 
                                               class="ajax interest-<?php echo Latte\Runtime\Filters::escapeHtml($interest->id_interest, ENT_COMPAT) ?>
 category-<?php echo Latte\Runtime\Filters::escapeHtml($category->id_category, ENT_COMPAT) ?>
" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("move!", array($interest->id_interest, $category->id_category)), ENT_COMPAT) ?>
">
                                                <?php echo Latte\Runtime\Filters::escapeHtml($category->name, ENT_NOQUOTES) ?>

                                            </a>
                                        </li>
<?php $iterations++; } ?>
                                    </ul>
                                    <button type="button" class="btn btn-link bg-blue contact-interest" id="contact-interest-<?php echo Latte\Runtime\Filters::escapeHtml($interest->id_interest, ENT_COMPAT) ?>
" title="<?php echo Latte\Runtime\Filters::escapeHtml($person->email, ENT_COMPAT) ?>">
<?php if ($interest->contacted!="") { ?>
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Contacted
<?php } else { ?>
                                            Contact
<?php } ?>
                                    </button>
                                    <div id="dialog-<?php echo Latte\Runtime\Filters::escapeHtml($interest->id_interest, ENT_COMPAT) ?>" class="dialog" title="Contact candidate">
<?php $_l->tmp = $_control->getComponent("contactForm"); if ($_l->tmp instanceof Nette\Application\UI\IRenderable) $_l->tmp->redrawControl(NULL, FALSE); $_l->tmp->render() ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
<?php $iterations++; } 
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIMacros::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 