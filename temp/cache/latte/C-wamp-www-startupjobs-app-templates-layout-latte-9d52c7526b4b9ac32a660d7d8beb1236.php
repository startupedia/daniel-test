<?php
// source: C:\wamp\www\startupjobs\app/templates/@layout.latte

// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('5482222587', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block head
//
if (!function_exists($_b->blocks['head'][] = '_lb362bc0e15b_head')) { function _lb362bc0e15b_head($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;
}}

//
// block scripts
//
if (!function_exists($_b->blocks['scripts'][] = '_lb1fc16f6f5a_scripts')) { function _lb1fc16f6f5a_scripts($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/jquery.js"></script>
    <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/netteForms.js"></script>
    <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/nette.ajax.js"></script>
    <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/spinner.ajax.js"></script>
    <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/jquery-ui/jquery-ui.min.js"></script>
                <!--[if IE]>
                <script src="<?php echo Latte\Runtime\Filters::escapeHtmlComment($basePath) ?>/js/html5.js"></script>
                <script src="<?php echo Latte\Runtime\Filters::escapeHtmlComment($basePath) ?>/js/respond.min.js"></script>
                <![endif]-->
    <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/main.js"></script>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIMacros::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php if (isset($_b->blocks["title"])) { ob_start(); Latte\Macros\BlockMacros::callBlock($_b, 'title', $template->getParameters()); echo $template->striptags(ob_get_clean()) ?>
 | <?php } ?>Startup Jobs</title>

        <link rel="stylesheet" media="screen,projection,tv" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/screen.css">
        <link rel="stylesheet" media="print" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/print.css">
        <link rel="shortcut icon" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/favicon.ico">
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
        <link href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/jquery-ui/jquery-ui.min.css">
        <link href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/custom.css" rel="stylesheet">
        <?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['head']), $_b, get_defined_vars())  ?>

    </head>

    <body>
        <script> document.documentElement.className += ' js'</script>
                <header role="banner">
            <div class="container">
                <div class="page-header">
                    <div class="header">
                        <nav class="navbar">
                            <a class="navbar-brand" href="#">
                                <img src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/images/logo1.png" width="241px" height="33px" alt="Be a shark, work for a startup!">
                            </a>
                            <ul class="nav nav-pills pull-right size-plus">
                                <li role="presentation">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        StartupJobs.com <span class="caret"></span>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <img src="https://media.licdn.com/mpr/mprx/0_RSNOt_VWOcHiw19ARW93t3VkY1gmWK9AMu8TthWzT9Wxnq3lBwk-p8jRKgjKoBcjVmnhgbchqCSA" width="36px" height="36px" class="img-circle profile-pic">
                                    <a href="#" class="dropdown-toggle inline" data-toggle="dropdown" role="button" aria-expanded="false">
                                        Jack Brown <span class="caret"></span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="jumbotron">
                <div class="container">
                    <h1>Administration <small>StartupJobs.com</small></h1>
                </div>
            </div>
            <div class="container bg-plus">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <nav class="navbar">
                        <ul class="nav navbar-nav nav-tabs pull-left nav2">
                            <li><a href="#" class="green">Add offer</a></li>
                            <li><a href="#">Offer <span class="badge">2</span></a></li>
                            <li class="active"><a href="#">Candidates <span class="badge"><?php echo Latte\Runtime\Filters::escapeHtml(count($interestsAll), ENT_NOQUOTES) ?></span></a></li>
                            <li><a href="#">Profile</a></li>
                            <li><a href="#">Settings</a></li>
                            <li><a href="#">Finance</a></li>
                        </ul>
                        <ul class="nav navbar-nav nav-tabs pull-right nav2">
                            <li><a href="#">Fast contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    <main class="size-plus">
<?php $iterations = 0; foreach ($flashes as $flash) { ?>        <div class="flash <?php echo Latte\Runtime\Filters::escapeHtml($flash->type, ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($flash->message, ENT_NOQUOTES) ?></div>
<?php $iterations++; } Latte\Macros\BlockMacros::callBlock($_b, 'content', $template->getParameters()) ?>
    </main>
    <nav class="navbar navbar-inverse nav-bottom">
        <div class="container">
            <p class="navbar-text navbar-left">© 2012 - 2015 Startupedia s.r.o</p>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="">Blog</a></li>
                <li><a href="">Offers</a></li> 
                <li><a href="">Startups</a></li> 
                <li><a href="">About StartupJobs</a></li>
                <li><a href="">VOP</a></li> 
                <li><a href="">For media</a></li>
                <li><a href="">Contact</a></li>
            </ul>
        </div>
    </nav>
<?php call_user_func(reset($_b->blocks['scripts']), $_b, get_defined_vars())  ?>
</body>
</html>
